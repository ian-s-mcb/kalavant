# Kalavant #
An [Ansible][ansible project page] script that automates the configuration of my Ubuntu laptop.

## Getting started ##
Kalavant can be executed in two modes: ssh or local connection. The second mode offers simplicity and runs faster, however it is currently unavailable because of a bug I ran into with Ansible. 

#### Kalavant via ssh connection ####
```bash
sudo apt-get install -y git python-dev python-pip openssh-server
sudo pip install -U pip
sudo pip install ansible

ssh-keygen
ssh-copy-id 127.0.0.1
sudo visudo
# add the following line to the bottom of the file:
# 'ian ALL=(ALL) NOPASSWD:ALL'
# where 'ian' is the username

# (if needed) create storage partition + mount point
# add entries to fstab for storage + homeStorage

git clone https://github.com/ian-s-mcb/kalavant
cd kalavant
wget https://bitbucket.org/ian_s_mcb/storage/downloads/deb_files.tar
tar xfv deb_files.tar
vi group_vars/all
# update key-value pairs as needed

ansible-playbook -i hosts.ssh site.yml
# wait an hour for script to finish
```

#### Kalavant via local connection ####
```bash
sudo apt-get install -y git python-dev python-pip
sudo pip install -U pip
sudo pip install ansible

sudo visudo
# add the following line to the bottom of the file:
# 'ian ALL=(ALL) NOPASSWD:ALL'
# where 'ian' is the username

# (if needed) create storage partition + mount point
# add entries to fstab for storage + homeStorage

git clone https://github.com/ian-s-mcb/kalavant
cd kalavant
wget https://bitbucket.org/ian_s_mcb/storage/downloads/deb_files.tar
tar xfv deb_files.tar
vi group_vars/all
# update key-value pairs as needed

ansible-playbook -i hosts.local site.yml
# wait < 1hr for script to finish
```

[ansible project page]: https://github.com/ansible/ansible
